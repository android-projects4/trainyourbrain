package com.example.mohnad.trainyourbrain;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainTrain extends Activity {
    TextView sumTextView, scoreTextView, resultTextView, timerTextView;
    ArrayList<Integer> answersArrayList = new ArrayList<Integer>();
    int locationOfCorrectAnswer;
    int score = 0;
    int numberOfQuestions = 0;
    Button button0, button1, button2, button3, playAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_train);
        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        playAgain = (Button) findViewById(R.id.playAgainButton);
        scoreTextView = (TextView) findViewById(R.id.scoreTextView);
        resultTextView = (TextView) findViewById(R.id.resultTextView);
        timerTextView = (TextView) findViewById(R.id.timerTextView);


        trainAgain(playAgain);
    }

    public void generateQuestion() {

        answersArrayList.clear();

        Random rand = new Random();

        int a = rand.nextInt(21);
        int b = rand.nextInt(21);

        sumTextView = (TextView) findViewById(R.id.questionTextView);

        sumTextView.setText(a + " + " + b);
        int wrong;
        locationOfCorrectAnswer = rand.nextInt(4);

        for (int i = 0; i < 4; i++) {
            if (i == locationOfCorrectAnswer) {
                answersArrayList.add(a + b);
            } else {
                wrong = rand.nextInt(41);
                while (wrong == a + b) {

                    wrong = rand.nextInt(41);

                }
                answersArrayList.add(wrong);
            }

        }

        button0.setText(Integer.toString(answersArrayList.get(0)));
        button1.setText(Integer.toString(answersArrayList.get(1)));
        button2.setText(Integer.toString(answersArrayList.get(2)));
        button3.setText(Integer.toString(answersArrayList.get(3)));
    }

    public void chooseAnswer(View view) {
        if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {
            score++;

            resultTextView.setText("Correct!");
        } else {
            resultTextView.setText("Wrong!");
        }
        numberOfQuestions++;
        scoreTextView.setText(score + "/" + numberOfQuestions);
        generateQuestion();
    }

    public void trainAgain(View view) {
        score = 0;
        numberOfQuestions = 0;
        timerTextView.setText("30s");
        scoreTextView.setText("0/0");
        resultTextView.setText(" ");
        playAgain.setVisibility(View.INVISIBLE);
        generateQuestion();
        new CountDownTimer(30000, 1000) {

            @Override
            public void onTick(long l) {
                timerTextView.setText((int) l / 1000 + "s");
            }

            @Override
            public void onFinish() {
                timerTextView.setText("0s");
                resultTextView.setText("Your Score: " + score + "/" + numberOfQuestions);
                playAgain.setVisibility(View.VISIBLE);
            }
        }.start();

    }

}
